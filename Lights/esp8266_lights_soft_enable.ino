#ifndef UNIT_TEST
#include <Arduino.h>
#endif

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>




/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "purrpurr"
#define WLAN_PASS       "12345678"

/************************* MQTT Broker Setup *********************************/
#define mqtt_server      "10.0.0.10"
#define mqtt_serverport  1883                   // use 8883 for SSL
#define mqtt_username    "user1"
#define mqtt_password    "1"

/************************* Constants, Variables, Integers, etc *********************************/
const int Lights[] = {D1, D2, D3};
const int VSensors[] = {D5, D6, D7};
int VSensorsPreviousState[] = {0, 0, 0};
const int LightsAutoPin = D0;
int AutoMode = 0;

const long togDelay = 100;  // pause for 100 milliseconds before toggling to Open
const long postDelay = 200;  // pause for 200 milliseconds after toggling to Open
int i = 0;


int LightState = LOW;
String command;
String state;


// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, mqtt_server, mqtt_serverport, mqtt_username, mqtt_password);

// Setup subscription 'Light' for monitoring topic for changes.
Adafruit_MQTT_Subscribe Light1 = Adafruit_MQTT_Subscribe(&mqtt, "lab/Lights/Light1/command");
Adafruit_MQTT_Subscribe Light2 = Adafruit_MQTT_Subscribe(&mqtt, "lab/Lights/Light2/command");
Adafruit_MQTT_Subscribe Light3 = Adafruit_MQTT_Subscribe(&mqtt, "lab/Lights/Light3/command");

Adafruit_MQTT_Publish Light1Publish = Adafruit_MQTT_Publish(&mqtt, "lab/Lights/Light1/command");
Adafruit_MQTT_Publish Light2Publish = Adafruit_MQTT_Publish(&mqtt, "lab/Lights/Light2/command");
Adafruit_MQTT_Publish Light3Publish = Adafruit_MQTT_Publish(&mqtt, "lab/Lights/Light3/command");

Adafruit_MQTT_Publish Light1State = Adafruit_MQTT_Publish(&mqtt, "lab/Lights/Light1/state");
Adafruit_MQTT_Publish Light2State = Adafruit_MQTT_Publish(&mqtt, "lab/Lights/Light2/state");
Adafruit_MQTT_Publish Light3State = Adafruit_MQTT_Publish(&mqtt, "lab/Lights/Light3/state");

Adafruit_MQTT_Subscribe LightsAuto = Adafruit_MQTT_Subscribe(&mqtt, "lab/Lights/LightsAuto/command");

Adafruit_MQTT_Publish Publish[] = {Light1Publish, Light2Publish, Light3Publish};
Adafruit_MQTT_Publish State[] = {Light1State, Light2State, Light3State};

/*************************** Sketch Code ************************************/
void MQTT_connect();

void setup() {


  Serial.begin(115200);
  Serial.println(""); Serial.println(F("Booting... v1.0"));
  

  pinMode(LightsAutoPin, OUTPUT);
  digitalWrite (LightsAutoPin, LOW);

  for (i = 0; i < 3; i++) {
    pinMode(Lights[i], OUTPUT);
    digitalWrite (Lights[i], LOW);
  }
  

  for (i = 0; i < 3; i++) {
    pinMode(VSensors[i], INPUT);
  }
  


  
  // Connect to WiFi access point.
  Serial.print("Connecting to "); Serial.println(WLAN_SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting in 5 secs...");
    delay(5000);
    ESP.restart();
  }
  



  // Setup MQTT subscription for Light feed.
  mqtt.subscribe(&Light1);
  mqtt.subscribe(&Light2);
  mqtt.subscribe(&Light3);
  mqtt.subscribe(&LightsAuto);

  // Begin OTA
  ArduinoOTA.setPort(8266); // Port defaults to 8266
  ArduinoOTA.setHostname("Light");   // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setPassword((const char *)"<pass>");   // No authentication by default

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("");
  Serial.println("Ready & WiFi connected");
  Serial.print("IP address: "); Serial.println(WiFi.localIP());

  delay (1000);
  
    ArduinoOTA.handle();
    
  // Connect to MQTT 
  MQTT_connect();
  if (mqtt.connected()) {
    Serial.println("Enabling auto mode...");
    for (i = 0; i < 3; i++) {
      digitalWrite(Lights[i], digitalRead (VSensors[i]));
      VSensorsPreviousState[i] = digitalRead (VSensors[i]);
      State[i].publish ((digitalRead( Lights[i] ) == 0) ? "off" : "on");
      delay( 100 );
      attachInterrupt(digitalPinToInterrupt(VSensors[i]), handleInterrupt, CHANGE);
      Serial.println("Vsensor[" + String(i) + "] state: " + String( digitalRead (VSensors[i])));
      Serial.println("Lights[" + String(i) + "] state: " + String( digitalRead (Lights[i])));       
    }
    Serial.println("Auto mode enabled");    
    digitalWrite (LightsAutoPin, HIGH);
    AutoMode = 1;
  } else {
    Serial.println("MQTT connection failed! Rebooting in 5 secs...");
    delay(5000);
    ESP.restart();
  }

  
}

void loop() {
  ArduinoOTA.handle();
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();
  
  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) {
    if (AutoMode == 1) { 
      if (subscription == &Light1) {
        command = String((char *)Light1.lastread);
        command.toLowerCase();
        // Serial.print(F("Got: ")); Serial.println(command); 

        if (command == "on")
        {
          digitalWrite (Lights[0], HIGH);
          State[0].publish ("on");
        }
        else if (command == "off")
        {
          digitalWrite (Lights[0], LOW);
          State[0].publish ("off");
        }
        else 
        {
          break;
        }
      }
      else if (subscription == &Light2) {
        command = String((char *)Light2.lastread);
        command.toLowerCase();
        // Serial.print(F("Got: ")); Serial.println(command); 

        if (command == "on")
        {
          digitalWrite (Lights[1], HIGH);
          State[1].publish ("on");
        }
        else if (command == "off")
        {
          digitalWrite (Lights[1], LOW);
          State[1].publish ("off");
        }
        else 
        {
          break;
        }
      }
      else if (subscription == &Light3) {
        command = String((char *)Light3.lastread);
        command.toLowerCase();
        // Serial.print(F("Got: ")); Serial.println(command); 

        if (command == "on")
        {
          digitalWrite (Lights[2], HIGH);
          State[2].publish ("on");
        }
        else if (command == "off")
        {
          digitalWrite (Lights[2], LOW);
          State[2].publish ("off");
        }
        else 
        {
          break;
        }
      }

    }
    if (subscription == &LightsAuto) {
      command = String((char *)LightsAuto.lastread);
      command.toLowerCase();
      // Serial.print(F("Got Auto mode: ")); Serial.println(command); 

      if (command == "on")
      {
        for (i = 0; i < 3; i++) {
          digitalWrite(Lights[i], digitalRead (VSensors[i]));
        }
        digitalWrite (LightsAutoPin, HIGH);
        AutoMode = 1;
      }
      else if (command == "off")
      {
        digitalWrite (LightsAutoPin, LOW);
        AutoMode = 0;
      }
      else 
      {
        break;
      }
    }

    else {break;}
    
  }

   /* Serial.print(F("Закончили выгребать сообщения.\n")); 
   for (i = 0; i < 3; i++) {
     Serial.println("Sensor " + String(i+1) + " state: "  + digitalRead(VSensors[i]));
   }*/
   
  /*for (i = 0; i < 3; i++) {
     Serial.println("Light " + String(i+1) + " state: "  + digitalRead(Lights[i]));
  }*/
  mqtt.ping();
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }
  
  Serial.print("Connecting to MQTT... ");
  
  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}



void handleInterrupt () {
      detachInterrupt(VSensors[0]);
      detachInterrupt(VSensors[1]);
      detachInterrupt(VSensors[2]);
      unsigned long Milliseconds = millis() + 50;  //debounce delay
      while (Milliseconds > millis());
      for (i = 0; i < 3; i++) {
        if ( VSensorsPreviousState[i] != digitalRead (VSensors[i])) {
          Publish[i].publish ((digitalRead( Lights[i] ) == 0) ? "on" : "off");
          Milliseconds = millis() + 10;  //debounce delay
          while (Milliseconds > millis());
          VSensorsPreviousState[i] = !VSensorsPreviousState[i] ;
        }
      }     

      attachInterrupt(digitalPinToInterrupt(VSensors[0]), handleInterrupt, CHANGE);
      attachInterrupt(digitalPinToInterrupt(VSensors[1]), handleInterrupt, CHANGE);
      attachInterrupt(digitalPinToInterrupt(VSensors[2]), handleInterrupt, CHANGE);
  
}
