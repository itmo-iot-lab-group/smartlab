  
/*
 * Dim - Light Shield Toggle v1.0
 * Closes the Light for 100 ms, then opens based on OH2 event
 * Light Shield transistor closes Light when D1 is HIGH
*/

#ifndef UNIT_TEST
#include <Arduino.h>
#endif

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>




/************************* WiFi Access Point *********************************/
#define WLAN_SSID       "purrpurr"
#define WLAN_PASS       "12345678"

/************************* MQTT Broker Setup *********************************/
#define mqtt_server      "10.0.0.10"
#define mqtt_serverport  1883                   // use 8883 for SSL
#define mqtt_username    "user1"
#define mqtt_password    "1"

/************************* Constants, Variables, Integers, etc *********************************/
const int Curtains[] = {D0, D1, D2, D3};
const int CurtainLeftUp = D0;
const int CurtainLeftDown = D1;
const int CurtainRightUp = D2;
const int CurtainRightDown = D3;


const long togDelay = 200;  // pause for 100 milliseconds before toggling to Open
const long postDelay = 200;  // pause for 200 milliseconds after toggling to Open
int i = 0;

String command;
String state;


// Create an ESP8266 WiFiClient class to connect to the MQTT server.
WiFiClient client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, mqtt_server, mqtt_serverport, mqtt_username, mqtt_password);

// Setup subscription 'Light' for monitoring topic for changes.
Adafruit_MQTT_Subscribe CurtainLeft = Adafruit_MQTT_Subscribe(&mqtt, "lab/Curtains/CurtainLeft/command");
Adafruit_MQTT_Subscribe CurtainRight = Adafruit_MQTT_Subscribe(&mqtt, "lab/Curtains/CurtainRight/command");

Adafruit_MQTT_Publish CurtainLeftPublish = Adafruit_MQTT_Publish(&mqtt, "lab/Curtains/CurtainLeft/command");
Adafruit_MQTT_Publish CurtainRightPublish = Adafruit_MQTT_Publish(&mqtt, "lab/Curtains/CurtainRight/command");


/*************************** Sketch Code ************************************/
void MQTT_connect();

void setup() {


  Serial.begin(115200);
  Serial.println(""); Serial.println(F("Booting... v1.0"));
  

  for (i = 0; i < 4; i++) {
    pinMode(Curtains[i], OUTPUT);
    digitalWrite (Curtains[i], LOW);
  }

  // Connect to WiFi access point.
  Serial.print("Connecting to "); Serial.println(WLAN_SSID);
  WiFi.mode(WIFI_STA);
  WiFi.begin(WLAN_SSID, WLAN_PASS);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting in 5 secs...");
    delay(5000);
    ESP.restart();
  }
  



  // Setup MQTT subscription for Light feed.
  mqtt.subscribe(&CurtainLeft);
  mqtt.subscribe(&CurtainRight);

  // Begin OTA
  ArduinoOTA.setPort(8266); // Port defaults to 8266
  ArduinoOTA.setHostname("Curtains");   // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setPassword((const char *)"<pass>");   // No authentication by default

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("");
  Serial.println("Ready & WiFi connected");
  Serial.print("IP address: "); Serial.println(WiFi.localIP());

  delay (1000);
  
    ArduinoOTA.handle();
    
  // Connect to MQTT 
  MQTT_connect();
  if (mqtt.connected()) {
    Serial.println("MQTT connected!");
  } else {
    Serial.println("MQTT connection failed! Rebooting in 5 secs...");
    delay(5000);
    ESP.restart();
  }

  
}

void loop() {
  ArduinoOTA.handle();
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.
  MQTT_connect();
  
  // this is our 'wait for incoming subscription packets' busy subloop
  // try to spend your time here

  Adafruit_MQTT_Subscribe *subscription;
  while ((subscription = mqtt.readSubscription(5000))) {

      if (subscription == &CurtainLeft) {
        command = String((char *)CurtainLeft.lastread);
        command.toLowerCase();
        Serial.print(F("CurtainLeft - Got: ")); Serial.println(command); 

        if (command == "0")
        {
          digitalWrite (CurtainLeftUp, HIGH);
          delay (togDelay);
          digitalWrite (CurtainLeftUp, LOW);
        }
        else if (command == "100")
        {
          digitalWrite (CurtainLeftDown, HIGH);
          delay (togDelay);
          digitalWrite (CurtainLeftDown, LOW);
        }
        else 
        {
          break;
        }
      }
      else if (subscription == &CurtainRight) {
        command = String((char *)CurtainRight.lastread);
        command.toLowerCase();
        Serial.print(F("CurtainRight - Got: ")); Serial.println(command); 

        if (command == "0")
        {
          digitalWrite (CurtainRightUp, HIGH);
          delay (togDelay);
          digitalWrite (CurtainRightUp, LOW);
        }
        else if (command == "100")
        {
          digitalWrite (CurtainRightDown, HIGH);
          delay (togDelay);
          digitalWrite (CurtainRightDown, LOW);
        }
        else 
        {
          break;
        }
      }
    else {break;}
    
  }

   /* Serial.print(F("Закончили выгребать сообщения.\n")); 
   for (i = 0; i < 3; i++) {
     Serial.println("Sensor " + String(i+1) + " state: "  + digitalRead(VSensors[i]));
   }*/
   
  /*for (i = 0; i < 3; i++) {
     Serial.println("Light " + String(i+1) + " state: "  + digitalRead(Lights[i]));
  }*/
  mqtt.ping();
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
void MQTT_connect() {
  int8_t ret;

  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }
  
  Serial.print("Connecting to MQTT... ");
  
  uint8_t retries = 3;
  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       delay(5000);  // wait 5 seconds
       retries--;
       if (retries == 0) {
         // basically die and wait for WDT to reset me
         while (1);
       }
  }
  Serial.println("MQTT Connected!");
}
